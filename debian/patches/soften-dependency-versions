Description: Soften upstream dependencies to match Debian availability
Author: Nick Morrott <knowledgejunkie@gmail.com>
Forwarded: not-needed
Last-Update: 2002-06-13
---
Index: mu-editor/setup.py
===================================================================
--- mu-editor.orig/setup.py
+++ mu-editor/setup.py
@@ -25,25 +25,26 @@ install_requires = [
     # The core 'install_requires' should only be things
     # which are needed for the main editor to function.
     #
-    "PyQt5==5.13.2"
+    "PyQt5>=5.13.2"
     + ';"arm" not in platform_machine and "aarch" not in platform_machine',
-    "QScintilla==2.11.3"
-    + ';"arm" not in platform_machine and "aarch" not in platform_machine',
-    "PyQtChart==5.13.1"
+    "QScintilla>=2.11.3"
     + ';"arm" not in platform_machine and "aarch" not in platform_machine',
+    # FOR DEBIAN : the package provides pyqt5.qtchart 5.15.7+dfsg-1
+    #"pyqt5.qtchart>=5.13.1"
+    #+ ';"arm" not in platform_machine and "aarch" not in platform_machine',
     # FIXME: jupyter-client added for Py3.5 compatibility, to be dropped after
-    # Mu v1.1 release. So, qtconsole < 5 and jupyter-client < 6.2 (issue #1444)
-    "jupyter-client>=4.1,<6.2",
+    # Mu v1.1 release. So, qtconsole < 5 and (NOT WITH DEBIAN) jupyter-client < 6.2 (issue #1444)
+    "jupyter-client>=4.1",
     # FIXME: ipykernel max added for macOS 10.13 compatibility, min taken from
     # qtconsole 4.7.7. Full line can be removed after Mu v1.1 release.
     # Dependency mirrored for user venv in mu/wheels/__init__.py
-    "ipykernel>=4.1,<6",
+    "ipykernel>=4.1",
     # FIXME: ipykernel<6 depends on ipython_genutils, but it isn't explicitly
     # declared as a dependency. It also depends on traitlets, which
     # incidentally brought ipython_genutils, but in v5.1 it was dropped, so as
     # a workaround we need to manually specify it here.
     "ipython_genutils>=0.2.0",
-    "qtconsole==4.7.7",
+    "qtconsole>=4.7.7",
     #
     # adafruit-board-toolkit is used to find serial ports and help identify
     # CircuitPython boards in the CircuitPython mode.
@@ -56,10 +57,10 @@ install_requires = [
     # though. Regarding these packages' versions, please refer to:
     # http://flake8.pycqa.org/en/latest/faq.html#why-does-flake8-use-ranges-for-its-dependencies
     "flake8 >= 3.8.3",
-    # Clamp click max version to workaround incompatibility with black<22.1.0
-    "click<=8.0.4",
-    "black>=19.10b0,<22.1.0;python_version>'3.5'",
-    "platformdirs>=2.0.0,<3.0.0",
+    # DON'T Clamp click max version to workaround incompatibility with black<22.1.0
+    "click",
+    "black>=19.10b0;python_version>'3.5'",
+    "platformdirs>=2.0.0",
     "semver>=2.8.0",
     # virtualenv vendors pip, we need at least pip v19.3 to install some
     # rust based dependencies. virtualenv >=v20 is required for the --symlinks
@@ -74,7 +75,7 @@ install_requires = [
     #
     # Needed to resolve an issue with paths in the user virtual environment
     #
-    "pywin32; sys_platform=='win32'",
+    "pywin32; sys_platform>='win32'",
 ]
 
 
@@ -128,7 +129,7 @@ setup(
         "mu.modes.api",
         "mu.wheels",
     ],
-    python_requires=">=3.5,<3.9",
+    python_requires=">=3.5",
     install_requires=install_requires,
     extras_require=extras_require,
     package_data={"mu.wheels": ["*.whl", "*.zip"]},
